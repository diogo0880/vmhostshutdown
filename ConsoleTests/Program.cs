﻿using System.Diagnostics;
using System.Text.RegularExpressions;

namespace ConsoleTests
{
    public class Program
    {
        public static void Main(string[] args)
        {
            Process process = new Process();
            process.StartInfo = new ProcessStartInfo("C:/Program Files/Oracle/VirtualBox/VBoxManage.exe");
            process.StartInfo.Arguments = "showvminfo \"Video Converter\"";// | Select-String -Pattern \"running\"";
            process.StartInfo.WindowStyle = ProcessWindowStyle.Hidden;
            process.StartInfo.UseShellExecute = false;
            process.StartInfo.RedirectStandardOutput = true;

            process.Start();

            string output = process.StandardOutput.ReadToEnd();
            process.WaitForExit();

            System.Console.WriteLine(output);
            
            // State check
            System.Console.WriteLine(output.Contains("running"));

            // Better State check
            Regex regex = new Regex(@"State:\s+running");
            System.Console.WriteLine(regex.IsMatch(output));

            System.Console.WriteLine("PRESS ANY KEY TO EXIT");
            System.Console.ReadKey();
        }
    }
}
