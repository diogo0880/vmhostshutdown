﻿namespace VMHostShutdown
{
    partial class MainWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainWindow));
            this.notifyContextMenu = new System.Windows.Forms.ContextMenuStrip(this.components);
            this.toogleVMAnalysisMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyContextMenuSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.openSettingsWindowMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.exitMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
            this.poollingTimer = new System.Windows.Forms.Timer(this.components);
            this.shutdownTimer = new System.Windows.Forms.Timer(this.components);
            this.notifyContextMenu.SuspendLayout();
            this.SuspendLayout();
            // 
            // notifyContextMenu
            // 
            this.notifyContextMenu.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toogleVMAnalysisMenuItem,
            this.notifyContextMenuSeparator1,
            this.openSettingsWindowMenuItem,
            this.exitMenuItem});
            this.notifyContextMenu.Name = "notifyContextMenu";
            this.notifyContextMenu.Size = new System.Drawing.Size(206, 76);
            this.notifyContextMenu.Text = "VM Host Shutdown";
            // 
            // toogleVMAnalysisMenuItem
            // 
            this.toogleVMAnalysisMenuItem.Name = "toogleVMAnalysisMenuItem";
            this.toogleVMAnalysisMenuItem.Size = new System.Drawing.Size(205, 22);
            this.toogleVMAnalysisMenuItem.Text = "Pause VM Status Pooling";
            // 
            // notifyContextMenuSeparator1
            // 
            this.notifyContextMenuSeparator1.Name = "notifyContextMenuSeparator1";
            this.notifyContextMenuSeparator1.Size = new System.Drawing.Size(202, 6);
            // 
            // openSettingsWindowMenuItem
            // 
            this.openSettingsWindowMenuItem.Name = "openSettingsWindowMenuItem";
            this.openSettingsWindowMenuItem.Size = new System.Drawing.Size(205, 22);
            this.openSettingsWindowMenuItem.Text = "Open Settings Window";
            // 
            // exitMenuItem
            // 
            this.exitMenuItem.Name = "exitMenuItem";
            this.exitMenuItem.Size = new System.Drawing.Size(205, 22);
            this.exitMenuItem.Text = "Exit";
            this.exitMenuItem.Click += new System.EventHandler(this.exitToolStripMenuItem_Click);
            // 
            // notifyIcon
            // 
            this.notifyIcon.BalloonTipTitle = "VM Host Shutdown";
            this.notifyIcon.ContextMenuStrip = this.notifyContextMenu;
            this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
            this.notifyIcon.Text = "VM Host Shutdown";
            this.notifyIcon.Visible = true;
            // 
            // poollingTimer
            // 
            this.poollingTimer.Tick += new System.EventHandler(this.poollingTimer_Tick);
            // 
            // shutdownTimer
            // 
            this.shutdownTimer.Tick += new System.EventHandler(this.shutdownTimer_Tick);
            // 
            // MainWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.CausesValidation = false;
            this.ClientSize = new System.Drawing.Size(256, 256);
            this.ControlBox = false;
            this.Enabled = false;
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "MainWindow";
            this.ShowIcon = false;
            this.ShowInTaskbar = false;
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VM Host Shutdown";
            this.WindowState = System.Windows.Forms.FormWindowState.Minimized;
            this.notifyContextMenu.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ContextMenuStrip notifyContextMenu;
        private System.Windows.Forms.ToolStripMenuItem exitMenuItem;
        private System.Windows.Forms.NotifyIcon notifyIcon;
        private System.Windows.Forms.ToolStripMenuItem toogleVMAnalysisMenuItem;
        private System.Windows.Forms.ToolStripMenuItem openSettingsWindowMenuItem;
        private System.Windows.Forms.Timer poollingTimer;
        private System.Windows.Forms.ToolStripSeparator notifyContextMenuSeparator1;
        private System.Windows.Forms.Timer shutdownTimer;
    }
}