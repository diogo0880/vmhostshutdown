﻿namespace VMHostShutdown
{
    partial class SettingsWindow
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(SettingsWindow));
            this.mainPanel = new System.Windows.Forms.Panel();
            this.appName = new System.Windows.Forms.Label();
            this.analysisSettingsBox = new System.Windows.Forms.GroupBox();
            this.vmNamePanel = new System.Windows.Forms.Panel();
            this.vmNameLabel = new System.Windows.Forms.Label();
            this.vmNameField = new System.Windows.Forms.TextBox();
            this.pollingIntervalPanel = new System.Windows.Forms.Panel();
            this.poolingIntervalField = new System.Windows.Forms.NumericUpDown();
            this.poolingIntervalLabel = new System.Windows.Forms.Label();
            this.poolingIntervalUnitLabel = new System.Windows.Forms.Label();
            this.nTriesPanel = new System.Windows.Forms.Panel();
            this.nTriesLabel = new System.Windows.Forms.Label();
            this.nTriesField = new System.Windows.Forms.NumericUpDown();
            this.shutdownSettingsBox = new System.Windows.Forms.GroupBox();
            this.shutdownDelayPanel = new System.Windows.Forms.Panel();
            this.shutdownDelayLabel = new System.Windows.Forms.Label();
            this.forceShutdownPanel = new System.Windows.Forms.Panel();
            this.forceShutdownLabel = new System.Windows.Forms.Label();
            this.forceShutdownField = new System.Windows.Forms.CheckBox();
            this.topPanel = new System.Windows.Forms.Panel();
            this.dragWindowPanel = new System.Windows.Forms.Panel();
            this.closeButton = new System.Windows.Forms.Label();
            this.bottomPanel = new System.Windows.Forms.TableLayoutPanel();
            this.cancelButton = new System.Windows.Forms.Button();
            this.applyButton = new System.Windows.Forms.Button();
            this.shutdownDelayField = new System.Windows.Forms.MaskedTextBox();
            this.mainPanel.SuspendLayout();
            this.analysisSettingsBox.SuspendLayout();
            this.vmNamePanel.SuspendLayout();
            this.pollingIntervalPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.poolingIntervalField)).BeginInit();
            this.nTriesPanel.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nTriesField)).BeginInit();
            this.shutdownSettingsBox.SuspendLayout();
            this.shutdownDelayPanel.SuspendLayout();
            this.forceShutdownPanel.SuspendLayout();
            this.topPanel.SuspendLayout();
            this.bottomPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // mainPanel
            // 
            this.mainPanel.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.mainPanel.Controls.Add(this.appName);
            this.mainPanel.Controls.Add(this.analysisSettingsBox);
            this.mainPanel.Controls.Add(this.shutdownSettingsBox);
            this.mainPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.mainPanel.Location = new System.Drawing.Point(0, 0);
            this.mainPanel.Name = "mainPanel";
            this.mainPanel.Size = new System.Drawing.Size(362, 296);
            this.mainPanel.TabIndex = 0;
            // 
            // appName
            // 
            this.appName.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.appName.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.appName.Location = new System.Drawing.Point(0, 20);
            this.appName.Name = "appName";
            this.appName.Size = new System.Drawing.Size(362, 57);
            this.appName.TabIndex = 1;
            this.appName.Text = "VM Host Shutdown";
            this.appName.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // analysisSettingsBox
            // 
            this.analysisSettingsBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.analysisSettingsBox.Controls.Add(this.vmNamePanel);
            this.analysisSettingsBox.Controls.Add(this.pollingIntervalPanel);
            this.analysisSettingsBox.Controls.Add(this.nTriesPanel);
            this.analysisSettingsBox.Location = new System.Drawing.Point(12, 80);
            this.analysisSettingsBox.Name = "analysisSettingsBox";
            this.analysisSettingsBox.Size = new System.Drawing.Size(338, 115);
            this.analysisSettingsBox.TabIndex = 1;
            this.analysisSettingsBox.TabStop = false;
            this.analysisSettingsBox.Text = "Analysis Settings";
            // 
            // vmNamePanel
            // 
            this.vmNamePanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.vmNamePanel.Controls.Add(this.vmNameLabel);
            this.vmNamePanel.Controls.Add(this.vmNameField);
            this.vmNamePanel.Location = new System.Drawing.Point(6, 19);
            this.vmNamePanel.Name = "vmNamePanel";
            this.vmNamePanel.Size = new System.Drawing.Size(326, 26);
            this.vmNamePanel.TabIndex = 0;
            // 
            // vmNameLabel
            // 
            this.vmNameLabel.AutoSize = true;
            this.vmNameLabel.Location = new System.Drawing.Point(3, 6);
            this.vmNameLabel.Name = "vmNameLabel";
            this.vmNameLabel.Size = new System.Drawing.Size(114, 13);
            this.vmNameLabel.TabIndex = 6;
            this.vmNameLabel.Text = "Virtual Machine Name:";
            this.vmNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // vmNameField
            // 
            this.vmNameField.Location = new System.Drawing.Point(143, 3);
            this.vmNameField.Name = "vmNameField";
            this.vmNameField.Size = new System.Drawing.Size(180, 20);
            this.vmNameField.TabIndex = 1;
            // 
            // pollingIntervalPanel
            // 
            this.pollingIntervalPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.pollingIntervalPanel.Controls.Add(this.poolingIntervalField);
            this.pollingIntervalPanel.Controls.Add(this.poolingIntervalLabel);
            this.pollingIntervalPanel.Controls.Add(this.poolingIntervalUnitLabel);
            this.pollingIntervalPanel.Location = new System.Drawing.Point(6, 51);
            this.pollingIntervalPanel.Name = "pollingIntervalPanel";
            this.pollingIntervalPanel.Size = new System.Drawing.Size(264, 26);
            this.pollingIntervalPanel.TabIndex = 1;
            // 
            // poolingIntervalField
            // 
            this.poolingIntervalField.DecimalPlaces = 1;
            this.poolingIntervalField.Location = new System.Drawing.Point(143, 3);
            this.poolingIntervalField.Maximum = new decimal(new int[] {
            3600,
            0,
            0,
            0});
            this.poolingIntervalField.Minimum = new decimal(new int[] {
            1,
            0,
            0,
            65536});
            this.poolingIntervalField.Name = "poolingIntervalField";
            this.poolingIntervalField.Size = new System.Drawing.Size(68, 20);
            this.poolingIntervalField.TabIndex = 0;
            this.poolingIntervalField.ThousandsSeparator = true;
            this.poolingIntervalField.Value = new decimal(new int[] {
            3600,
            0,
            0,
            0});
            // 
            // poolingIntervalLabel
            // 
            this.poolingIntervalLabel.AutoSize = true;
            this.poolingIntervalLabel.Location = new System.Drawing.Point(3, 6);
            this.poolingIntervalLabel.Name = "poolingIntervalLabel";
            this.poolingIntervalLabel.Size = new System.Drawing.Size(83, 13);
            this.poolingIntervalLabel.TabIndex = 7;
            this.poolingIntervalLabel.Text = "Pooling Interval:";
            this.poolingIntervalLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // poolingIntervalUnitLabel
            // 
            this.poolingIntervalUnitLabel.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.poolingIntervalUnitLabel.AutoSize = true;
            this.poolingIntervalUnitLabel.Location = new System.Drawing.Point(214, 6);
            this.poolingIntervalUnitLabel.Name = "poolingIntervalUnitLabel";
            this.poolingIntervalUnitLabel.Size = new System.Drawing.Size(47, 13);
            this.poolingIntervalUnitLabel.TabIndex = 8;
            this.poolingIntervalUnitLabel.Text = "seconds";
            // 
            // nTriesPanel
            // 
            this.nTriesPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink;
            this.nTriesPanel.Controls.Add(this.nTriesLabel);
            this.nTriesPanel.Controls.Add(this.nTriesField);
            this.nTriesPanel.Location = new System.Drawing.Point(6, 83);
            this.nTriesPanel.Name = "nTriesPanel";
            this.nTriesPanel.Size = new System.Drawing.Size(196, 26);
            this.nTriesPanel.TabIndex = 3;
            // 
            // nTriesLabel
            // 
            this.nTriesLabel.AutoSize = true;
            this.nTriesLabel.Location = new System.Drawing.Point(3, 6);
            this.nTriesLabel.Name = "nTriesLabel";
            this.nTriesLabel.Size = new System.Drawing.Size(85, 13);
            this.nTriesLabel.TabIndex = 7;
            this.nTriesLabel.Text = "Number of Tries:";
            this.nTriesLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // nTriesField
            // 
            this.nTriesField.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.nTriesField.Location = new System.Drawing.Point(143, 3);
            this.nTriesField.Name = "nTriesField";
            this.nTriesField.Size = new System.Drawing.Size(50, 20);
            this.nTriesField.TabIndex = 3;
            this.nTriesField.Value = new decimal(new int[] {
            100,
            0,
            0,
            0});
            // 
            // shutdownSettingsBox
            // 
            this.shutdownSettingsBox.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Bottom | System.Windows.Forms.AnchorStyles.Left)));
            this.shutdownSettingsBox.Controls.Add(this.shutdownDelayPanel);
            this.shutdownSettingsBox.Controls.Add(this.forceShutdownPanel);
            this.shutdownSettingsBox.Location = new System.Drawing.Point(12, 201);
            this.shutdownSettingsBox.Name = "shutdownSettingsBox";
            this.shutdownSettingsBox.Size = new System.Drawing.Size(338, 83);
            this.shutdownSettingsBox.TabIndex = 5;
            this.shutdownSettingsBox.TabStop = false;
            this.shutdownSettingsBox.Text = "Shutdown Settings";
            // 
            // shutdownDelayPanel
            // 
            this.shutdownDelayPanel.Controls.Add(this.shutdownDelayLabel);
            this.shutdownDelayPanel.Controls.Add(this.shutdownDelayField);
            this.shutdownDelayPanel.Location = new System.Drawing.Point(6, 19);
            this.shutdownDelayPanel.Name = "shutdownDelayPanel";
            this.shutdownDelayPanel.Size = new System.Drawing.Size(196, 26);
            this.shutdownDelayPanel.TabIndex = 5;
            // 
            // shutdownDelayLabel
            // 
            this.shutdownDelayLabel.AutoSize = true;
            this.shutdownDelayLabel.Location = new System.Drawing.Point(3, 6);
            this.shutdownDelayLabel.Name = "shutdownDelayLabel";
            this.shutdownDelayLabel.Size = new System.Drawing.Size(88, 13);
            this.shutdownDelayLabel.TabIndex = 4;
            this.shutdownDelayLabel.Text = "Shutdown Delay:";
            this.shutdownDelayLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // forceShutdownPanel
            // 
            this.forceShutdownPanel.Controls.Add(this.forceShutdownLabel);
            this.forceShutdownPanel.Controls.Add(this.forceShutdownField);
            this.forceShutdownPanel.Location = new System.Drawing.Point(6, 51);
            this.forceShutdownPanel.Name = "forceShutdownPanel";
            this.forceShutdownPanel.Size = new System.Drawing.Size(161, 26);
            this.forceShutdownPanel.TabIndex = 6;
            // 
            // forceShutdownLabel
            // 
            this.forceShutdownLabel.AutoSize = true;
            this.forceShutdownLabel.Location = new System.Drawing.Point(3, 6);
            this.forceShutdownLabel.Name = "forceShutdownLabel";
            this.forceShutdownLabel.Size = new System.Drawing.Size(88, 13);
            this.forceShutdownLabel.TabIndex = 0;
            this.forceShutdownLabel.Text = "Force Shutdown:";
            this.forceShutdownLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // forceShutdownField
            // 
            this.forceShutdownField.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.forceShutdownField.AutoSize = true;
            this.forceShutdownField.Location = new System.Drawing.Point(143, 6);
            this.forceShutdownField.Name = "forceShutdownField";
            this.forceShutdownField.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.forceShutdownField.Size = new System.Drawing.Size(15, 14);
            this.forceShutdownField.TabIndex = 5;
            this.forceShutdownField.UseVisualStyleBackColor = true;
            // 
            // topPanel
            // 
            this.topPanel.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.topPanel.BackgroundImageLayout = System.Windows.Forms.ImageLayout.None;
            this.topPanel.Controls.Add(this.dragWindowPanel);
            this.topPanel.Controls.Add(this.closeButton);
            this.topPanel.Dock = System.Windows.Forms.DockStyle.Top;
            this.topPanel.Location = new System.Drawing.Point(0, 0);
            this.topPanel.Margin = new System.Windows.Forms.Padding(0);
            this.topPanel.Name = "topPanel";
            this.topPanel.Size = new System.Drawing.Size(362, 20);
            this.topPanel.TabIndex = 6;
            // 
            // dragWindowPanel
            // 
            this.dragWindowPanel.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.dragWindowPanel.Location = new System.Drawing.Point(0, 0);
            this.dragWindowPanel.Name = "dragWindowPanel";
            this.dragWindowPanel.Size = new System.Drawing.Size(342, 20);
            this.dragWindowPanel.TabIndex = 3;
            this.dragWindowPanel.MouseDown += new System.Windows.Forms.MouseEventHandler(this.dragWindowPanel_MouseDown);
            this.dragWindowPanel.MouseLeave += new System.EventHandler(this.dragWindowPanel_MouseLeave);
            this.dragWindowPanel.MouseMove += new System.Windows.Forms.MouseEventHandler(this.dragWindowPanel_MouseMove);
            this.dragWindowPanel.MouseUp += new System.Windows.Forms.MouseEventHandler(this.dragWindowPanel_MouseUp);
            // 
            // closeButton
            // 
            this.closeButton.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.closeButton.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.closeButton.Location = new System.Drawing.Point(342, 0);
            this.closeButton.Margin = new System.Windows.Forms.Padding(0);
            this.closeButton.Name = "closeButton";
            this.closeButton.Size = new System.Drawing.Size(20, 20);
            this.closeButton.TabIndex = 6;
            this.closeButton.Text = "X";
            this.closeButton.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.closeButton.Click += new System.EventHandler(this.closeButton_Click);
            // 
            // bottomPanel
            // 
            this.bottomPanel.AutoSize = true;
            this.bottomPanel.BackColor = System.Drawing.SystemColors.ControlDarkDark;
            this.bottomPanel.ColumnCount = 2;
            this.bottomPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.bottomPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.bottomPanel.Controls.Add(this.cancelButton, 0, 0);
            this.bottomPanel.Controls.Add(this.applyButton, 1, 0);
            this.bottomPanel.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.bottomPanel.GrowStyle = System.Windows.Forms.TableLayoutPanelGrowStyle.AddColumns;
            this.bottomPanel.Location = new System.Drawing.Point(0, 296);
            this.bottomPanel.Name = "bottomPanel";
            this.bottomPanel.RowCount = 1;
            this.bottomPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.bottomPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.bottomPanel.Size = new System.Drawing.Size(362, 29);
            this.bottomPanel.TabIndex = 0;
            // 
            // cancelButton
            // 
            this.cancelButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.cancelButton.Location = new System.Drawing.Point(53, 3);
            this.cancelButton.Name = "cancelButton";
            this.cancelButton.Size = new System.Drawing.Size(75, 23);
            this.cancelButton.TabIndex = 0;
            this.cancelButton.Text = "Cancel";
            this.cancelButton.UseVisualStyleBackColor = true;
            this.cancelButton.Click += new System.EventHandler(this.cancelButton_Click);
            // 
            // applyButton
            // 
            this.applyButton.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.applyButton.Location = new System.Drawing.Point(234, 3);
            this.applyButton.MaximumSize = new System.Drawing.Size(75, 23);
            this.applyButton.MinimumSize = new System.Drawing.Size(75, 23);
            this.applyButton.Name = "applyButton";
            this.applyButton.Size = new System.Drawing.Size(75, 23);
            this.applyButton.TabIndex = 1;
            this.applyButton.Text = "Apply";
            this.applyButton.UseVisualStyleBackColor = true;
            this.applyButton.Click += new System.EventHandler(this.applyButton_Click);
            // 
            // shutdownDelayField
            // 
            this.shutdownDelayField.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.shutdownDelayField.Location = new System.Drawing.Point(143, 3);
            this.shutdownDelayField.Mask = "00:00:00";
            this.shutdownDelayField.Name = "shutdownDelayField";
            this.shutdownDelayField.Size = new System.Drawing.Size(49, 20);
            this.shutdownDelayField.TabIndex = 4;
            this.shutdownDelayField.Text = "000000";
            this.shutdownDelayField.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // SettingsWindow
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(362, 325);
            this.ControlBox = false;
            this.Controls.Add(this.topPanel);
            this.Controls.Add(this.mainPanel);
            this.Controls.Add(this.bottomPanel);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.MinimizeBox = false;
            this.Name = "SettingsWindow";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "VM Host Shutdown - Settings";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.SettingsWindow_FormClosing);
            this.mainPanel.ResumeLayout(false);
            this.analysisSettingsBox.ResumeLayout(false);
            this.vmNamePanel.ResumeLayout(false);
            this.vmNamePanel.PerformLayout();
            this.pollingIntervalPanel.ResumeLayout(false);
            this.pollingIntervalPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.poolingIntervalField)).EndInit();
            this.nTriesPanel.ResumeLayout(false);
            this.nTriesPanel.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.nTriesField)).EndInit();
            this.shutdownSettingsBox.ResumeLayout(false);
            this.shutdownDelayPanel.ResumeLayout(false);
            this.shutdownDelayPanel.PerformLayout();
            this.forceShutdownPanel.ResumeLayout(false);
            this.forceShutdownPanel.PerformLayout();
            this.topPanel.ResumeLayout(false);
            this.bottomPanel.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Panel mainPanel;
        private System.Windows.Forms.Label appName;
        private System.Windows.Forms.GroupBox analysisSettingsBox;
        private System.Windows.Forms.TextBox vmNameField;
        private System.Windows.Forms.GroupBox shutdownSettingsBox;
        private System.Windows.Forms.CheckBox forceShutdownField;
        private System.Windows.Forms.Label vmNameLabel;
        private System.Windows.Forms.Label poolingIntervalLabel;
        private System.Windows.Forms.Panel vmNamePanel;
        private System.Windows.Forms.Panel pollingIntervalPanel;
        private System.Windows.Forms.Panel shutdownDelayPanel;
        private System.Windows.Forms.Label shutdownDelayLabel;
        private System.Windows.Forms.Panel forceShutdownPanel;
        private System.Windows.Forms.Label forceShutdownLabel;
        private System.Windows.Forms.Panel topPanel;
        private System.Windows.Forms.Label closeButton;
        private System.Windows.Forms.Panel dragWindowPanel;
        private System.Windows.Forms.Panel nTriesPanel;
        private System.Windows.Forms.NumericUpDown nTriesField;
        private System.Windows.Forms.Label nTriesLabel;
        private System.Windows.Forms.Label poolingIntervalUnitLabel;
        private System.Windows.Forms.NumericUpDown poolingIntervalField;
        private System.Windows.Forms.TableLayoutPanel bottomPanel;
        private System.Windows.Forms.Button cancelButton;
        private System.Windows.Forms.Button applyButton;
        private System.Windows.Forms.MaskedTextBox shutdownDelayField;
    }
}