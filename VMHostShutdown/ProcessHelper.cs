﻿using System.Diagnostics;

namespace VMHostShutdown
{
    internal static class ProcessHelper
    {
        public static ProcessStartInfo GetVirtualBoxVMInfo()
        {
            // TODO Adicionar o caminho para a VM como parametro configuravel
            // TODO Adicionar deteção automática do caminho para a VM

            var startInfo = new ProcessStartInfo(@"C:\Program Files\Oracle\VirtualBox\VBoxManage.exe");
            startInfo.Arguments = "showvminfo \"Video Converter\"";
            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.UseShellExecute = false;
            startInfo.CreateNoWindow = true;
            startInfo.RedirectStandardOutput = true;

            return startInfo;
        }

        public static void ShutdownSystem(bool force = true)
        {
            var startInfo = new ProcessStartInfo("shutdown");

            startInfo.Arguments = $"-s -p";
            if (force)
                startInfo.Arguments += " -f";

            startInfo.WindowStyle = ProcessWindowStyle.Hidden;
            startInfo.UseShellExecute = false;
            startInfo.CreateNoWindow = true;
            startInfo.RedirectStandardOutput = false;

            new Process { StartInfo = startInfo }.Start();
        }
    }
}
