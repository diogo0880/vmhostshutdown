﻿using System;
using System.Diagnostics;
using System.Drawing;
using System.Drawing.Imaging;
using System.Text.RegularExpressions;
using System.Windows.Forms;

using VMHostShutdown.Properties;

namespace VMHostShutdown
{
    public partial class MainWindow : Form
    {
        private const int PROCESS_TO_POOL_WAIT_DURATION = 5000;
        private const int NOTIFY_ICON_BALLOON_DURATION = 5000;

        private Process processToPool;
        private Regex regexStateMatcher;

        private int retryCounter;

        public MainWindow()
        {
            InitializeComponent();

            regexStateMatcher = new Regex(@"State:\s+running");
            poollingTimer.Interval = 1000;

            MessageBox.Show($"VirtualMachineName: {Settings.Default.VirtualMachineName}\nPoolingInterval: {Settings.Default.PoolingInterval}\nNumberOfRetries: {Settings.Default.NumberOfTries}\nShutdownDelay: {Settings.Default.ShutdownDelay}\nForceShutdown: {Settings.Default.ForceShutdown}");

            if (!Settings.Default.IsComplete)
                OpenSettingsWindow(true);

            StartPooling();
        }

        private void OpenSettingsWindow(bool waitForClose = false)
        {
            SettingsWindow settingsWindow = new SettingsWindow();

            if (waitForClose)
                settingsWindow.ShowDialog();
            else
                settingsWindow.Show();
        }

        private void exitToolStripMenuItem_Click(object sender, EventArgs e) => Application.Exit();

        #region VM State Pooling
        private void StartPooling()
        {
            retryCounter = 0;
            poollingTimer.Interval = Convert.ToInt32(Math.Round(Settings.Default.PoolingInterval * 1000));
            poollingTimer.Start();

            notifyIcon.BalloonTipTitle = $"VM Host Shutdown ({Settings.Default.VirtualMachineName})";
            notifyIcon.BalloonTipText = "VM state pooling has been started.";
            notifyIcon.ShowBalloonTip(NOTIFY_ICON_BALLOON_DURATION);
        }

        private void poollingTimer_Tick(object sender, EventArgs e)
        {
            processToPool = new Process();
            processToPool.StartInfo = ProcessHelper.GetVirtualBoxVMInfo();
            processToPool.Start();

            processToPool.WaitForExit(PROCESS_TO_POOL_WAIT_DURATION);
            if (regexStateMatcher.IsMatch(processToPool.StandardOutput.ReadToEnd()))
            {
                retryCounter = 0;
            }
            else
            {
                if (++retryCounter == Settings.Default.NumberOfTries)
                {
                    poollingTimer.Stop();
                    ShutdownSystem();
                }
            }
        }
        #endregion

        #region System Shutdown
        private void ShutdownSystem()
        {
            int secondsToShutdown = Convert.ToInt32(Math.Round(Settings.Default.ShutdownDelay.TotalSeconds));

            shutdownTimer.Interval = secondsToShutdown * 1000;
            shutdownTimer.Start();

            DialogResult dialogResult = MessageBox.Show($"System will shut down in {secondsToShutdown} seconds.\nAny unsaved file may be lost.",
                "System Shutdown", MessageBoxButtons.OKCancel, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button1);

            switch (dialogResult)
            {
                case DialogResult.Cancel:
                    shutdownTimer.Stop();
                    break;
            }
        }

        private void shutdownTimer_Tick(object sender, EventArgs e)
        {
            shutdownTimer.Stop();

            ProcessHelper.ShutdownSystem();
        }
        #endregion
    }
}
