﻿using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

using VMHostShutdown.Properties;

namespace VMHostShutdown
{
    public partial class SettingsWindow : Form
    {
        Point? mouseMovementReference;

        private string ClosingWarningMessage
        {
            get
            {
                return string.Concat("Closing this window without setting up",
                    Environment.NewLine, "the application will shut it down!",
                    Environment.NewLine, "Do you wanna quit?");
            }
        }

        public SettingsWindow()
        {
            InitializeComponent();

            shutdownDelayField.ValidatingType = typeof(TimeSpan);

            LoadValues();
        }

        private void LoadValues()
        {
            vmNameField.Text = Settings.Default.VirtualMachineName;
            poolingIntervalField.Value = Convert.ToDecimal(Settings.Default.PoolingInterval);
            nTriesField.Value = Settings.Default.NumberOfTries;

            shutdownDelayField.Text = Settings.Default.ShutdownDelay.ToString();
            forceShutdownField.Checked = Settings.Default.ForceShutdown;
        }

        #region Actions
        #region Window Movement Actions
        private void dragWindowPanel_MouseMove(object sender, MouseEventArgs e)
        {
            if (mouseMovementReference.HasValue)
            {
                Point temp = GetWindowPositionFromMouseMovement(e.Location);
                vmNameField.Text = string.Concat(temp.ToString());
                Location = temp;
            }
        }

        ///
        /// <param name="mousePosition">Position of the mouse cursor, using the window as reference.</param>
        /// 
        private Point GetWindowPositionFromMouseMovement(Point mousePosition)
        {
            int xCoordinate = Location.X + mousePosition.X - mouseMovementReference.Value.X;
            int yCoordinate = Location.Y + mousePosition.Y - mouseMovementReference.Value.Y;

            xCoordinate = Math.Max(0, xCoordinate);
            yCoordinate = Math.Max(0, yCoordinate);

            //int totalScreenWidth = -Size.Width;
            //int totalScreenHeight = -Size.Height;
            //foreach (Screen screen in Screen.AllScreens)
            //{
            //    int screenX = screen.WorkingArea.X;
            //    int screenY = screen.WorkingArea.Y;
            //    int screenWidth = screen.WorkingArea.Width;
            //    int screenHeight = screen.WorkingArea.Height;

            //    totalScreenWidth += screenWidth;
            //    totalScreenHeight += screenHeight;

            //    MessageBox.Show(string.Concat(screenWidth, ", ", screenHeight, Environment.NewLine, screenX, ", ", screenY), "Screen " + screen.DeviceName);
            //}

            //MessageBox.Show(string.Concat(totalScreenWidth, ", ", totalScreenHeight), "Total");

            //xCoordinate = Math.Min(totalScreenWidth, xCoordinate);
            //yCoordinate = Math.Min(totalScreenHeight, yCoordinate);

            vmNameField.Text = Screen.AllScreens.Sum(screen => screen.Bounds.Width).ToString();

            return new Point(xCoordinate, yCoordinate);
        }

        private void dragWindowPanel_MouseDown(object sender, MouseEventArgs e)
        {
            mouseMovementReference = e.Location;
        }

        private void dragWindowPanel_MouseUp(object sender, MouseEventArgs e)
        {
            dragWindowPanel_MouseLeave(sender, e);
        }

        private void dragWindowPanel_MouseLeave(object sender, EventArgs e)
        {
            mouseMovementReference = null;
        }
        #endregion

        #region Apply Actions
        private void applyButton_Click(object sender, EventArgs e)
        {
            Settings.Default.VirtualMachineName = vmNameField.Text;
            Settings.Default.PoolingInterval = (float)poolingIntervalField.Value;
            Settings.Default.NumberOfTries = (uint)nTriesField.Value;

            Settings.Default.ShutdownDelay = TimeSpan.Parse(shutdownDelayField.Text);
            Settings.Default.ForceShutdown = forceShutdownField.Checked;

            Settings.Default.Save();

            Close();
        }
        #endregion

        #region Close Actions
        private void SettingsWindow_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (e.CloseReason != CloseReason.UserClosing)
                return;

            if (!Settings.Default.IsComplete)
                ShowClosingWarningAndHandle(e);
        }

        private void closeButton_Click(object sender, EventArgs e)
        {
            Close();
        }

        private void cancelButton_Click(object sender, EventArgs e)
        {
            Close();
        }
        #endregion
        #endregion

        private void ShowClosingWarningAndHandle(FormClosingEventArgs e)
        {
            DialogResult result = MessageBox.Show(ClosingWarningMessage, "Application Configuration",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2);

            switch (result)
            {
                case DialogResult.No:
                    e.Cancel = true;
                    break;

                case DialogResult.Yes:
                    Application.Exit();
                    break;
            }
        }
    }
}
